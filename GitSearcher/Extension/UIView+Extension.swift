//
//  UIView+Extension.swift
//  GitSearcher
//
//  Created by David Angkasa on 17/09/22.
//

import UIKit

extension UIView {
    func setupSubviews(_ views: [UIView]) {
        for item in views {
            addSubview(item)
        }
    }
    
    public func setLayer(cornerRadius: CGFloat? = nil, borderWidth width: CGFloat? = nil, borderColor color: UIColor? = nil, clipToBounds: Bool = true) {
        setNeedsLayout()
        layoutIfNeeded()
        if let radius = cornerRadius {
            let size = (frame.width == 0 ? frame.height : frame.width) / 2
            layer.cornerRadius = (radius == 0 ? size : radius)
        } else {
            layer.cornerRadius = 0
        }
        
        
        if let width = width {
            layer.borderWidth = width
        }
        if let color = color {
            layer.borderColor = color.cgColor
        }
        
        layer.masksToBounds = clipToBounds
    }
}
