//
//  UIAppearance.swift
//  GitSearcher
//
//  Created by David Angkasa on 17/09/22.
//
import UIKit

extension UIAppearance {
    func style(_ completion: @escaping ((Self)->Void)) {
        completion(self)
    }
}
