//
//  UserListInteractor.swift
//  GitSearcher
//
//  Created by David Angkasa on 17/09/22.
//

import Foundation

class UserListInteractor: UserListInteractorDelegate {
    
    let presenter: UserListPresenterDelegate?
    
    init(presenter: UserListPresenterDelegate) {
        self.presenter = presenter
    }
    
    func getUserList(since: Int) {
        guard let url = URL(string: "http://api.github.com/users?since=\(since)") else {
            return
        }
        
        NetworkManager().fetchRequest(type: [UserModel].self, url: url) { response in
            switch response {
            case .success(let model):
                if !model.isEmpty {
                    self.presenter?.getUserListOnSuccess(data: model)
                } else {
                    self.presenter?.getUserListOnFailure(reason: .NoData)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func searchUsers(query: String, since: Int) {
        guard let url = URL(string: "http://api.github.com/search/users?q=\(query)&since=\(since)&per_page=30") else {
            return
        }
        
        NetworkManager().fetchRequest(type: SearchModel.self, url: url) { response in
            switch response {
            case .success(let model):
                if ((model.totalCount ?? 0) != 0) {
                    self.presenter?.searchUsersOnSuccess(data: model)
                } else {
                    self.presenter?.searchUsersOnFailure(reason: .NoData)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
