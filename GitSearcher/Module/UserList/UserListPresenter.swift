//
//  UserListPresenter.swift
//  GitSearcher
//
//  Created by David Angkasa on 17/09/22.
//

import Foundation

class UserListPresenter: UserListPresenterDelegate {
    var view: UserListViewDelegate?
    lazy var interactor: UserListInteractorDelegate = UserListInteractor(presenter: self)
    
    init(view: UserListViewDelegate) {
        self.view = view
    }
    
    func getUserList(since: Int) {
        interactor.getUserList(since: since)
    }
    
    func getUserListOnSuccess(data: [UserModel]) {
        view?.getUserListOnSuccess(data: data)
    }
    
    func getUserListOnFailure(reason: CustomError) {
        view?.getUserListOnFailure(reason: reason)
    }
    
    func searchUsers(query: String, since: Int) {
        interactor.searchUsers(query: query, since: since)
    }
    
    func searchUsersOnSuccess(data: SearchModel) {
        view?.searchUsersOnSuccess(data: data)
    }
    
    func searchUsersOnFailure(reason: CustomError) {
        view?.searchUsersOnFailure(reason: reason)
    }
}
