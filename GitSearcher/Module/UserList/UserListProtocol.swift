//
//  UserListProtocol.swift
//  GitSearcher
//
//  Created by David Angkasa on 17/09/22.
//

import Foundation

protocol UserListViewDelegate: AnyObject {
    func getUserListOnSuccess(data: [UserModel])
    func getUserListOnFailure(reason: CustomError)
    
    func searchUsersOnSuccess(data: SearchModel)
    func searchUsersOnFailure(reason: CustomError)
}

protocol UserListPresenterDelegate: AnyObject {
    func getUserList(since: Int)
    func getUserListOnSuccess(data: [UserModel])
    func getUserListOnFailure(reason: CustomError)
    
    func searchUsers(query: String, since: Int)
    func searchUsersOnSuccess(data: SearchModel)
    func searchUsersOnFailure(reason: CustomError)
}

protocol UserListInteractorDelegate: AnyObject {
    func getUserList(since: Int)
    func searchUsers(query: String, since: Int)
}

protocol UserListRouterDelegate: AnyObject {
    
}
