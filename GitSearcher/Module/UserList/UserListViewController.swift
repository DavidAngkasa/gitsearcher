//
//  UserListViewController.swift
//  GitSearcher
//
//  Created by David Angkasa on 17/09/22.
//

import UIKit

class UserListViewController: UIViewController {
    let root = UserListView()
    lazy var presenter: UserListPresenterDelegate = UserListPresenter(view: self)
    var users: [UserModel] = []
    
    var page: Int = 1
    var totalSearch: Int = 0
    var isSearching: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view = root
        
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        setTableDelegate()
        root.searchBar.delegate = self
        presenter.getUserList(since: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addSearchToNavigation()
        removeNavBottomLine()
    }
    
    func addSearchToNavigation() {
        let searchBar = self.root.searchBar
        let rightNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.rightBarButtonItem = rightNavBarButton
    }
    
    func removeNavBottomLine() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(rgb: 0x0091b4)
        appearance.shadowColor = .clear
        appearance.shadowImage = UIImage()
        navigationController?.navigationBar.standardAppearance = appearance;
        navigationController?.navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
    }
    
    func setTableDelegate() {
        root.userListTV.delegate = self
        root.userListTV.dataSource = self
    }
}

extension UserListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
        if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
            let spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))

            self.root.userListTV.tableFooterView = spinner
            self.root.userListTV.tableFooterView?.isHidden = false
            
            if isSearching {
                if totalSearch > users.count {
                    self.presenter.searchUsers(query: self.root.searchBar.text ?? "", since: self.users.last?.id ?? 0)
                } else {
                    self.root.userListTV.tableFooterView?.isHidden = true
                }
            } else {
                self.presenter.getUserList(since: self.users.last?.id ?? 0)
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UserCell
        cell.initialize(user: users[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension UserListViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let text = searchBar.text ?? ""
        isSearching = true
        users = []
        presenter.searchUsers(query: text, since: 0)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        users = []
        presenter.getUserList(since: 0)
        searchBar.text = ""
        searchBar.endEditing(true)
    }
}

extension UserListViewController: UserListViewDelegate {
    func getUserListOnSuccess(data: [UserModel]) {
        self.users += data
        DispatchQueue.main.async { [weak self] in
            self?.root.userListTV.isHidden = false
            self?.root.noUserFoundLbl.isHidden = true
            self?.root.userListTV.reloadData()
            self?.root.userListTV.tableFooterView?.isHidden = true
        }
    }
    
    func getUserListOnFailure(reason: CustomError) {
        if reason == .NoData {
            DispatchQueue.main.async { [weak self] in
                self?.root.userListTV.isHidden = true
                self?.root.noUserFoundLbl.isHidden = false
            }
        }
    }
    
    func searchUsersOnSuccess(data: SearchModel) {
        self.totalSearch = data.totalCount ?? 0
        self.users += data.items ?? []
        DispatchQueue.main.async { [weak self] in
            self?.root.userListTV.isHidden = false
            self?.root.noUserFoundLbl.isHidden = true
            self?.root.userListTV.reloadData()
            self?.root.userListTV.tableFooterView?.isHidden = true
        }
    }
    
    func searchUsersOnFailure(reason: CustomError) {
        if reason == .NoData {
            DispatchQueue.main.async { [weak self] in
                self?.root.userListTV.isHidden = true
                self?.root.noUserFoundLbl.isHidden = false
            }
        }
    }
}
