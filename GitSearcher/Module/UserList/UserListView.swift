//
//  UserListView.swift
//  GitSearcher
//
//  Created by David Angkasa on 17/09/22.
//

import UIKit
import SnapKit

class UserListView: UIView {
    let userListTV = UITableView()
    let searchBar = UISearchBar()
    
    let spaceBar = UIView()
    
    let noUserFoundLbl = UILabel()
    
    override init(frame: CGRect) {
        super .init(frame: mainScreen)
        backgroundColor = .white
        
        setupSubviews([userListTV, searchBar, spaceBar, noUserFoundLbl])
//        cardListTV.setupSubviews([searchController.searchBar])
        
        setupConstraints()
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView() {
        userListTV.style {
            $0.register(UserCell.self, forCellReuseIdentifier: "cell")
            $0.backgroundColor = .clear
        }
        
        searchBar.style {
            $0.sizeToFit()
            $0.placeholder = "Search"
            $0.searchTextField.backgroundColor = .white
            $0.showsCancelButton = true
            $0.autocapitalizationType = .none
        }
        
        spaceBar.backgroundColor = UIColor(rgb: 0x0091b4)
        
        noUserFoundLbl.style {
            $0.text = "No User Found."
            $0.font = UIFont.systemFont(ofSize: 22, weight: .bold)
            $0.textColor = .lightGray
            $0.isHidden = false
        }
    }
    
    func setupConstraints() {
        spaceBar.snp.makeConstraints { make in
            make.leading.trailing.equalTo(self)
            make.height.equalTo(8)
            make.top.equalTo(self.safeAreaLayoutGuide)
        }
        
        userListTV.snp.makeConstraints { make in
            make.top.equalTo(spaceBar.snp.bottom)
            make.bottom.equalTo(self.safeAreaLayoutGuide)
            make.leading.equalTo(self).offset(12)
            make.trailing.equalTo(self).offset(-12)
        }
        
        noUserFoundLbl.snp.makeConstraints { make in
            make.center.equalTo(self)
        }
    }
}
