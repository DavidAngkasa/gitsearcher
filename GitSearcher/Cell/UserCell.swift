//
//  UserCell.swift
//  GitSearcher
//
//  Created by David Angkasa on 17/09/22.
//

import UIKit
import Kingfisher
import SnapKit

class UserCell: UITableViewCell {
    let userImgView = UIImageView()
    let userNameLbl = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.setupSubviews([userImgView, userNameLbl])
        
        setupConstraints()
        setupView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userImgView.image = nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(user: UserModel) {
        guard let url = URL(string: user.avatarUrl ?? "") else { return }
//        loadImg(from: url)
        userImgView.kf.setImage(with: url)
        
        userNameLbl.text = user.login
    }
    
    func setupView() {
        userImgView.style {
            $0.setLayer(cornerRadius: 0)
            $0.image = UIImage(named: "ic_pikachu")
        }
        
        userNameLbl.style {
            $0.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
            $0.text = "Pikachu"
        }
    }
    
    func setupConstraints() {
        userImgView.snp.makeConstraints { make in
            make.size.equalTo(50)
            make.centerY.equalTo(contentView)
            make.leading.equalTo(contentView).offset(8)
        }
        
        userNameLbl.snp.makeConstraints { make in
            make.leading.equalTo(userImgView.snp.trailing).offset(20)
            make.centerY.equalTo(userImgView)
            make.trailing.equalTo(contentView).offset(-8)
        }
    }
}
