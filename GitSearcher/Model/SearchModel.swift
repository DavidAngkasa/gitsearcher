//
//  SearchModel.swift
//  GitSearcher
//
//  Created by David Angkasa on 22/09/22.
//

import Foundation

struct SearchModel: Codable {
    enum Category: String, Decodable {
        case swift, combine, debugging, xcode
    }
    
    enum CodingKeys: String, CodingKey {
        case items
        case incompleteResults = "incomplete_results"
        case totalCount = "total_count"
    }
    
    let totalCount: Int?
    let incompleteResults: Bool?
    let items: [UserModel]?
}
