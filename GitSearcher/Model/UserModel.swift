//
//  UserListModel.swift
//  GitSearcher
//
//  Created by David Angkasa on 21/09/22.
//

import Foundation

struct UserModel: Codable {
    enum Category: String, Decodable {
        case swift, combine, debugging, xcode
    }
    
    enum CodingKeys: String, CodingKey {
        case id, login
        case avatarUrl = "avatar_url"
    }
    
    let id: Int?
    let login: String?
    let avatarUrl: String?
}
